import 'package:flutter/material.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/screens/edit_profile.dart';
import 'package:me2us/screens/wrapper.dart';
import 'package:me2us/services/auth.dart';
import 'package:provider/provider.dart';

final ThemeData _themeData = new ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.lightGreen,
    accentColor: Colors.amber);

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  return runApp(
      MultiProvider(
        providers: [
          StreamProvider<User>.value(value: AuthService().user),
        ],
        child: MaterialApp(
          theme: _themeData,
          routes: {
            '/': (context) => Wrapper(),
            '/edit-profile': (context) => EditProfile(),
          },
        ),
      ));
}
