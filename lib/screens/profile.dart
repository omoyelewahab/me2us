import 'package:flutter/material.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/photos.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/social_media_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/screens/reg_screens/basic_info.dart';
import 'package:me2us/screens/reg_screens/contact_info.dart';
import 'package:me2us/screens/reg_screens/employment_info.dart';
import 'package:me2us/screens/reg_screens/marital_info.dart';
import 'package:me2us/screens/reg_screens/other_info.dart';
import 'package:me2us/screens/reg_screens/personal_info.dart';
import 'package:me2us/screens/reg_screens/photo_info.dart';
import 'package:me2us/screens/reg_screens/preferences.dart';
import 'package:me2us/screens/reg_screens/social_media_info.dart';
import 'package:me2us/screens/reg_screens/terms.dart';
import 'package:me2us/services/auth.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  // Basic Information Form
  _pushInfoForm(Widget form, String title, String uid) {
    MaterialPageRoute route = MaterialPageRoute(builder: (context) {
      return StreamProvider<UserProfileStatus>.value(
        value: DbService(uid).userProfileStatus,
        child: Scaffold(
          appBar: AppBar(
            title: Text(title),
            centerTitle: true,
          ),
          body: form,
        ),
      );
    });

    Navigator.push(context, route);
  }

  Widget _infoSectionStatus(var info){
    if(info == null) {
      return Text(
        'Loading...',
        style:
        TextStyle(fontSize: 14, color: Colors.lightGreenAccent),
      );
    } else if(info.runtimeType != UserProfileStatus){
      if(info.isSaved){
        return Text(
          'Saved',
          style: TextStyle(
              fontSize: 14, color: Colors.lightGreenAccent),
        );
      } else {
        return Text(
          'Not Saved',
          style: TextStyle(fontSize: 14, color: Colors.red[200]),
        );
      }
    } else {
      if(info.submitted){
        return Text(
          'Accepted',
          style: TextStyle(
              fontSize: 14, color: Colors.lightGreenAccent),
        );
      } else {
        return Text(
          'Not Accepted',
          style: TextStyle(fontSize: 14, color: Colors.red[200]),
        );
      }
    }


  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: AuthService().user,
      builder: ((context, snapshot) {
        if(snapshot.hasData) {
          User user = snapshot.data;
          String uid = user.uid;
          List<ListTile> tiles = [
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Basic Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<BasicInfo>(context)),
              onTap: () {
                return _pushInfoForm(BasicInfoForm(uid), 'Basic Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Contact Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<ContactInfo>(context)),
              onTap: () {
                return _pushInfoForm(ContactInfoForm(uid), 'Contact Information',uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Marital Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<MaritalInfo>(context)),
              onTap: () {
                return _pushInfoForm(MaritalInfoForm(uid), 'Marital Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Employment Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<EmploymentInfo>(context)),
              onTap: () {
                return _pushInfoForm(EmploymentInfoForm(uid), 'Employment Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Social Media Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<SocialMediaInfo>(context)),
              onTap: () {
                return _pushInfoForm(SocialMediaInfoForm(uid), 'Social Media Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Personal Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<PersonalInfo>(context)),
              onTap: () {
                return _pushInfoForm(PersonalInfoForm(uid), 'Personal Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Preferences Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<Preferences>(context)),
              onTap: () {
                return _pushInfoForm(PreferencesForm(uid), 'Preferences Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Other Information',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<OtherInfo>(context)),
              onTap: () {
                return _pushInfoForm(OtherInfoForm(uid), 'Other Information', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Upload Photos',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<PhotosInfo>(context)),
              onTap: () {
                return _pushInfoForm(PhotoInfoForm(uid), 'Photo uploads', uid);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.add,
                size: 40,
                color: Colors.amber,
              ),
              title: Text(
                'Terms and Condition',
                style: TextStyle(fontSize: 20),
              ),
              subtitle: _infoSectionStatus(Provider.of<UserProfileStatus>(context)),
              onTap: () {
                return _pushInfoForm(TermsAndConditions(uid), 'Accept terms', uid);
              },
            ),
          ];

          final List<Widget> dividedTiles = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return Scaffold(
            appBar: AppBar(
              title: Text('Update Profile Sections'),
              centerTitle: true,
            ),
            body: ListView(
              children: dividedTiles,
            ),
          );
        } else {
          if(snapshot.hasError) {
            return ConnectionError();
          } else {
            return Loading();
          }
        }
      }),
    );
  }
}
