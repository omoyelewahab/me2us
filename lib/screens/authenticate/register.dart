import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:me2us/services/auth.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/loading.dart';

class Register extends StatefulWidget {
  Function toggleView;

  Register(this.toggleView);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String _email = '';
  String _password = '';
  String error = '';

  String _validateEmail(){
    return EmailValidator.validate(_email.trim()) ? null : 'Invalid email';
  }

  String _validatePassword(){
    return _password.length > 6 ? null: 'The password too short';
  }

  final _formKey = GlobalKey<FormState>();
  final _authService = new AuthService();
  bool showLoading = false;

  @override
  Widget build(BuildContext context) {
    final AuthService _authService = new AuthService();

    return showLoading ? Loading() : Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Register',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
              SizedBox(height: 30,),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(
                          hintText: 'Email',
                          suffixIcon: Icon(Icons.email)
                      ),
                      autofocus: true,
                      onChanged: (value) {
                        setState(() {
                          _email = value.trim();
                        });
                      },
                      validator: (value) => this._validateEmail(),
                    ),
                    SizedBox(height: 20,),
                    TextFormField(
                      decoration: InputDecoration(
                          hintText: 'Password',
                          suffixIcon: Icon(Icons.lock)
                      ),
                      obscureText: true,
                      onChanged: (value) {
                        setState(() {
                          _password = value.trim();
                        });

                      },
                      validator: (value) => this._validatePassword(),
                    ),
                    SizedBox(height: 20,),
                    FlatButton(
                      color: Colors.white,
                      child: Text(
                        'Create an account',
                        style: TextStyle(
                          color: Colors.grey[800],
                        ),
                      ),
                      onPressed: () async {
                        FocusScope.of(context).unfocus();
                        if(_formKey.currentState.validate()){
                          setState(() {
                            showLoading = true;
                          });
                          dynamic result = await _authService.registerWithEmail(_email, _password);

                          if (result == null) {
                            setState(() {
                              showLoading = false;
                              error = 'Duplicate email: The email entered is already registered';
                            });
                          }
                        } else print('$_email $_password');
                      },
                    ),
                    Text(
                      error,
                      style: TextStyle(color: Colors.red[600], fontSize: 12),
                    ),

                  ],

                ),
              ),
              SizedBox(height: 20,),
              FlatButton(
                padding: EdgeInsets.zero,
                child: Text('Already have an account? Sign-In'),
                onPressed: (){
                  widget.toggleView();
                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}
