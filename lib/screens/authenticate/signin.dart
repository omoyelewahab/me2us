import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:me2us/services/auth.dart';
import 'package:me2us/shared/loading.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SignIn extends StatefulWidget {
  Function toggleView;

  SignIn(Function view) {this.toggleView = view;}

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String email = '';
  String password = '';
  bool isLoading = false;

  String _validateEmail() {
    return email.length == 0 ? 'Enter an email' :
    EmailValidator.validate(email.trim()) ? null: 'Enter a valid email';
  }

  String _validatePassword() {
    return password.length == 0 ? 'Enter a password' :
    password.length > 6 ? null : 'Password is too short';
  }

  final _formKey = GlobalKey<FormState>();
  String error = '';

  @override
  Widget build(BuildContext context) {

    final AuthService _authService = new AuthService();
    return isLoading ? Loading() : SafeArea(
      child: Scaffold(
        body: ListView(
          children: <Widget>[
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Sign In',
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                  SizedBox(height: 30,),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.email),
                            hintText: 'Email'
                          ),
                          autofocus: true,
                          onChanged: (value) {
                            setState(() {
                              email = value.trim();
                            });
                          },
                          validator: (value) => _validateEmail(),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: 'Password',
                            suffixIcon: Icon(Icons.lock)
                          ),
                          obscureText: true,
                          onChanged: (value) {
                            setState(() {
                              password = value.trim();
                            });
                          },
                          validator: (value) => _validatePassword(),
                        ),
                        SizedBox(height: 20,),
                        FlatButton(
                          color: Colors.white,
                          child: Text(
                            'Sign-In',
                            style: TextStyle(
                              color: Colors.grey[800],
                            ),
                          ),
                          onPressed: () async {
                            FocusScope.of(context).unfocus();
                            if(_formKey.currentState.validate()) {
                              setState(() {
                                isLoading = true;
                              });
                              dynamic result = await _authService.signIn(email, password);
                              print(result);
                              if (result == null) {
                                setState(() {
                                  error = 'Invalid credentials: Check email and Password';
                                  isLoading = false;

                                  // error = 'Invalid credentials: Check emal or password';
                                });
                              }
                            }
                          },
                        ),

                        Text(
                          error,
                          style: TextStyle(color: Colors.red[600], fontSize: 12),
                        ),
                      ],

                    ),
                  ),
                  SizedBox(height: 20,),
                  FlatButton(
                    padding: EdgeInsets.zero,
                    child: Text('New user? Register'),
                    onPressed: (){
                      widget.toggleView();
                    },
                  ),
                  FlatButton(
                    padding: EdgeInsets.zero,
                    child: Text('Forgotten password?', style: TextStyle(color: Colors.grey[300]),),
                    onPressed: (){

                    },
                  )

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
