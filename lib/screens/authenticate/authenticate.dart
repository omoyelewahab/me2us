import 'package:flutter/material.dart';
import 'package:me2us/screens/authenticate/register.dart';
import 'package:me2us/screens/authenticate/signin.dart';


class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool _showSignIn = true;

  void toggleView(){
    setState(() {
      this._showSignIn = !_showSignIn;
    });
  }
  @override
  Widget build(BuildContext context) {
    if(_showSignIn) {
      return SignIn(this.toggleView);
    }else{
      return Register(this.toggleView);
    }
  }
}
