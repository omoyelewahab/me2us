import 'package:flutter/material.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/social_media_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/screens/home.dart';
import 'package:me2us/services/db_service.dart';
import 'package:provider/provider.dart';

class HomeWrapper extends StatefulWidget {
  @override
  _HomeWrapperState createState() => _HomeWrapperState();
}

class _HomeWrapperState extends State<HomeWrapper> {
  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User>(context).uid;

    return MultiProvider(
      providers: [
        StreamProvider<BasicInfo>.value(value: DbService(uid).basicInfo),
        StreamProvider<ContactInfo>.value(value: DbService(uid).contactInfo),
        StreamProvider<EmploymentInfo>.value(value: DbService(uid).employmentInfo),
        StreamProvider<MaritalInfo>.value(value: DbService(uid).maritalInfo),
        StreamProvider<OtherInfo>.value(value: DbService(uid).otherInfo),
        StreamProvider<PersonalInfo>.value(value: DbService(uid).personalInfo),
        StreamProvider<Preferences>.value(value: DbService(uid).preferenceInfo),
        StreamProvider<SocialMediaInfo>.value(value: DbService(uid).socialMediaInfo),
      ],
        child: Home()
    );
  }
}
