import 'package:flutter/material.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class EmploymentInfoForm extends StatefulWidget {
  final String uid;


  EmploymentInfoForm(this.uid);

  @override
  _EmploymentInfoFormState createState() => _EmploymentInfoFormState();
}

class _EmploymentInfoFormState extends State<EmploymentInfoForm> {
  final _formKey = GlobalKey<FormState>();
  String _employmentStatus;
  String _occupation;
  String _placeOfWork;
  String _workAddress1;
  String _workAddress2;
  String _annualSalaryRange;

  bool _showLoading = false;

  List<DropdownMenuItem> _getEmploymentStatus() {
    return [
      DropdownMenuItem(value: 'Employed', child: Text('Employed (Salary)')),
      DropdownMenuItem(value: 'Self Employed', child: Text('Self Employed')),
      DropdownMenuItem(value: 'Unemployed', child: Text('Unemployed')),
    ];
  }
  List<DropdownMenuItem> _getSalaryRange() {
    return [
      DropdownMenuItem(value: 'Less than 1M', child: Text('Below 1M Naira')),
      DropdownMenuItem(value: '1M - 3M', child: Text('1M to 3M Naira')),
      DropdownMenuItem(value: '3M - 5M', child: Text('3M to 5M Naira')),
      DropdownMenuItem(value: '5M - 7M', child: Text('5M to 7M Naira')),
      DropdownMenuItem(value: '7M - 10M', child: Text('7M to 10M Naira')),
      DropdownMenuItem(value: 'Above 10M', child: Text('Above 10M Naira')),
    ];
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return StreamBuilder(
      stream: DbService(user.uid).employmentInfo,
      builder: ((context, snapshot) {
        if(snapshot.hasData) {
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          EmploymentInfo employmentInfo = snapshot.data;
          return _showLoading ? Loading() : ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        DropdownButtonFormField(
                          hint: Text('Employment Status'),
                          value: employmentInfo.employmentStatus ??
                              _employmentStatus,
                          items: _getEmploymentStatus(),
                          onChanged: (val) {
                            setState(() {
                              _employmentStatus = val;
                            });
                          },
                          validator: (val){return val == null ? 'Select an employment status' : null;},
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: employmentInfo.occupation ?? null,
                          onChanged: (val) {
                            setState(() {
                              _occupation = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Occupation',
                            hintText: 'Enter your occupation',
                          ),
                          validator: (val) {
                            return val.length < 3 ? 'Enter your occupation' : null;
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: employmentInfo.placeOfWork ?? null,
                          onChanged: (val) {
                            setState(() {
                              _placeOfWork = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Place of work',
                            hintText: 'Enter your company\'s name',
                          ),
                          validator: (val) {
                            return val.length < 3 ? 'Enter your company\'s name' : null;
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: employmentInfo.workAddress1 ?? null,
                          onChanged: (val) {
                            setState(() {
                              _workAddress1 = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Work Address',
                            hintText: 'Enter your company\'s address',
                          ),
                          validator: (val) {
                            return val.length < 3 ? 'Enter your company\'s address' : null;
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: employmentInfo.workAddress2 ?? null,
                          onChanged: (val) {
                            setState(() {
                              _workAddress2 = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Work Address (Line 2)',
                            hintText: 'Enter your company\'s address',
                          ),
                        ),
                        // Phone Number
                        DropdownButtonFormField(
                          hint: Text('Annual Salary'),
                          value: employmentInfo.annualSalaryRange ??
                              _annualSalaryRange,
                          items: _getSalaryRange(),
                          onChanged: (val) {
                            setState(() {
                              _annualSalaryRange = val;
                            });
                          },
                          validator: (val){return val == null ? 'Select a salary range' : null;},
                        ),
                        SizedBox(height: 50),
                        MaterialButton(
                          minWidth: double.infinity,
                          color: Colors.amber,
                          child: Text(
                            'UPDATE EMPLOYMENT INFORMATION',
                            style: TextStyle(color: Colors.black),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                _showLoading = true;
                              });
                              var newEmploymentInfo =
                              EmploymentInfo(user.uid);
                              newEmploymentInfo.employmentStatus = _employmentStatus ?? employmentInfo.employmentStatus;
                              newEmploymentInfo.occupation = _occupation ?? employmentInfo.occupation;
                              newEmploymentInfo.placeOfWork = _placeOfWork ?? employmentInfo.placeOfWork;
                              newEmploymentInfo.workAddress1 = _workAddress1 ?? employmentInfo.workAddress1;
                              newEmploymentInfo.workAddress2 = _workAddress2 ?? employmentInfo.workAddress2;
                              newEmploymentInfo.annualSalaryRange = _annualSalaryRange ?? employmentInfo.annualSalaryRange;
                              newEmploymentInfo.isSaved = true;
                              dynamic result =
                              await DbService(user.uid)
                                  .updateEmploymentInfo(
                                  newEmploymentInfo);

                              if(profileStatus!= null){
                                profileStatus.employmentInfo = true;
                                await DbService(user.uid).updateUserProfileStatusInfo(profileStatus);
                              }

                              if (result != null) {
                                print('Request done');
                                print(result.toString());
                                setState(() {
                                  _showLoading = false;
                                });
                              } else {
                                Navigator.pop(context);
                              }
                            }
                          },
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        } else if (snapshot.hasError) {
          return ConnectionError();
        } else{
          return Loading();
        }
      }),
    );
  }
}
