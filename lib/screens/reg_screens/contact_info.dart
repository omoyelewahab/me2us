import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class ContactInfoForm extends StatefulWidget {
  final String uid;

  ContactInfoForm(this.uid);

  @override
  _ContactInfoFormState createState() => _ContactInfoFormState(uid);
}

class _ContactInfoFormState extends State<ContactInfoForm> {
  final uid;


  _ContactInfoFormState(this.uid);

  String _email;
  String _phoneNumber;
  String _address1;
  String _address2;
  String _countryOfResidence = 'Nigeria';
  bool _isSaved = false;
  bool showLoading = false;
  bool _basedOutsideNig = false;

  String _validateEmail(String email){
    return EmailValidator.validate(email) ? null : 'Invalid email';
  }

  String _validatePhoneNumber(String phoneNumber){
    return (phoneNumber.length < 1) ? 'Enter a phone number' : phoneNumber.length < 11 ? 'Enter a mobile phone number' : null;
  }

  String _validateAddress(String address){
    return (address.length < 8) ? 'Address seems too short' : null;
  }

  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    var user = Provider.of<User>(context);

    return StreamBuilder<ContactInfo>(
      stream: DbService(user.uid).contactInfo,
      builder: ((context, snapshot){
        if(snapshot.hasData){
          var contactInfo = snapshot.data;
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          // var _basedOutsideNig = contactInfo.countryOfResidence != 'Nigeria';
          return showLoading ? Loading() : ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        // Email
                        TextFormField(
                          initialValue: contactInfo.email ?? user.email ??null,
                          onChanged: (val) {
                            setState(() {
                              _email = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Email Address',
                            hintText: 'Enter your email',
                          ),
                          validator: (val) {
                            return _validateEmail(val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        // Phone Number
                        TextFormField(
                          initialValue: contactInfo.phoneNumber ?? null,
                          onChanged: (val) {
                            setState(() {
                              _phoneNumber = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Phone Number',
                            hintText: 'Enter your phone number',
                          ),
                          validator: (val) {
                            return _validatePhoneNumber(val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        // Address 1
                        TextFormField(
                          initialValue: contactInfo.address1 ?? null,
                          onChanged: (val) {
                            setState(() {
                              _address1 = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Address Line 1',
                            hintText: 'Enter your address (Line 1)',
                          ),
                          validator: (val) {
                            return _validateAddress(val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        // Address 2
                        TextFormField(
                          initialValue: contactInfo.address2 ?? null,
                          onChanged: (val) {
                            setState(() {
                              _address2 = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Address Line 2',
                            hintText: 'Address line 2',
                          ),
                        ),
                        SizedBox(height: 20),
                        //Middle Name
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('I live outside Nigeria'),
                            Checkbox(
                              value: _basedOutsideNig,
                              activeColor: Colors.amber,
                              onChanged: (bool value){
                                setState(() {
                                  _basedOutsideNig = value;
                                });
                              },
                            )

                          ],
                        ),
                        _basedOutsideNig ?
                        TextFormField(
                          initialValue: contactInfo.countryOfResidence ?? null,
                          onChanged: (val) {
                            setState(() {
                              _countryOfResidence = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Country of Residence',
                            hintText: 'Country of Residence',
                          ),
                          validator: (val) {return val.length < 3 && _basedOutsideNig ? 'Country name too short': null;},
                        ) : Container(),

                        SizedBox(height: 50),
                        MaterialButton(
                          minWidth: double.infinity,
                          color: Colors.amber,
                          child: Text('UPDATE CONTACT INFORMATION', style: TextStyle(color: Colors.black),),
                          onPressed: () async {
                            if(_formKey.currentState.validate()){
                              setState(() {
                                showLoading = true;
                              });
                              print(_email);
                              var contactInfo2 = ContactInfo(user.uid);
                              contactInfo2.email = _email ?? user.email ??contactInfo.email;
                              contactInfo2.phoneNumber = _phoneNumber ?? contactInfo.phoneNumber;
                              contactInfo2.address1 = _address1 ?? contactInfo.address1;
                              contactInfo2.address2 = _address2 ?? contactInfo.address2;
                              contactInfo2.countryOfResidence = _countryOfResidence ?? contactInfo.countryOfResidence;
                              contactInfo2.isSaved = true;
                              dynamic result = await DbService(user.uid).updateContactInfo(contactInfo2);
                              if(profileStatus!= null){
                                profileStatus.contactInfo = true;
                                await DbService(uid).updateUserProfileStatusInfo(profileStatus);
                              }

                              if(result != null) {
                                setState(() {
                                  showLoading = false;
                                });
                              } else {
                                Navigator.pop(context);
                              }
                            }
                          },
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        }else if(snapshot.hasError) {
          return ConnectionError();
        }else{
          return Loading();
        }
      }),
    );
  }
}
