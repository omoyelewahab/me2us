import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/alerts.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';

class TermsAndConditions extends StatefulWidget {
  final String uid;

  TermsAndConditions(this.uid);

  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState(uid);
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  String uid;
  bool showLoading = false;
  bool accepted = false;
  final TextStyle _style = TextStyle(
    fontStyle: FontStyle.normal,
    fontSize: 16,
  );

  bool _isCompleted(UserProfileStatus status) {
    if (status.basicInfo &&
        status.contactInfo &&
        status.employmentInfo &&
        status.maritalInfo &&
        status.otherInfo &&
        status.personalInfo &&
        status.preferences &&
        status.socialMediaInfo &&
        status.photoInfo) {
      return true;
    } else
      return false;
  }

  _TermsAndConditionsState(this.uid);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DbService(uid).userProfileStatus,
      builder: ((context, snapshot) {
        if (snapshot.hasData) {
          var statusInfo = snapshot.data;
          if (_isCompleted(statusInfo)) {
            return showLoading
                ? Loading()
                : ListView(
                    children: <Widget>[
                      Container(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                          child: Column(
                            children: <Widget>[
                              RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(children: [
                                  TextSpan(
                                      text:
                                          'Aminat\'s Secret LTD shall not be liable for any loss or damage of any sort arising out of the use and/or temporary or permanent discontinuation of AsMe2us services. Notwithstanding anything to the contrary contained herein, ASMe2Us, liability to you for any cause whatsoever, and regardless of the form of the action, will at all times be limited to the amount paid, if any, by you to Aminat\'s Secret LTD, for the Service during the term of membership.',
                                      style: _style),
                                  TextSpan(
                                      text:
                                          'As a Member of AsMe2us you are warned against disclosure of any personal / confidential / sensitive information to any other member, previous or current employee of Aminat\'s Secret LTD. As a member you would do so solely at your own risk and Aminat\'s Secret LTD shall not be liable for the outcome of any such transaction of information including and not limited to damages for loss of profits or savings, business interruption, loss of information or repute.',
                                      style: _style),
                                  TextSpan(
                                      text:
                                          'That there are certain free and paid features which may not be incorporated in the mobile application version though they are a part of the original website since the mobile apps are designed to be compact and light to ensure a smooth experience. Each Member hereby represents, warrants and agrees that the same shall be acceptable as a matter of fact and no requests to incorporate any such feature in the mobile app version would be initiated by the Members.',
                                      style: _style),
                                ]),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('I agree to these terms'),
                                  Checkbox(
                                    value: accepted,
                                    activeColor: Colors.amber,
                                    onChanged: (bool value) {
                                      setState(() {
                                        accepted = value;
                                      });
                                    },
                                  )
                                ],
                              ),
                              SizedBox(height: 50),
                              MaterialButton(
                                minWidth: double.infinity,
                                color: Colors.amber,
                                child: Text(
                                  'SUBMIT PROFILE',
                                  style: TextStyle(color: Colors.black),
                                ),
                                onPressed: () async {
                                  if (accepted) {
                                    setState(() {
                                      showLoading = true;
                                    });
                                    statusInfo.submitted = true;
                                    var result = await DbService(uid)
                                        .updateUserProfileStatusInfo(
                                            statusInfo);
                                    if (result != null) {
                                      setState(() {
                                        showLoading = false;
                                      });
                                    } else {
                                      Navigator.pop(context);
                                    }
                                  } else {
                                    Alerts().basicAlert(context, 'Accept the terms');
                                    return;
                                  }
                                },
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
          } else {
            return StatusNotCompleted();
          }
        } else if (snapshot.hasError) {
          return ConnectionError();
        } else {
          return Loading();
        }
      }),
    );
  }
}
