import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/alerts.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class BasicInfoForm extends StatefulWidget {
  final String uid;
  BasicInfoForm(this.uid);
  @override
  _BasicInfoFormState createState() => _BasicInfoFormState();
}

class _BasicInfoFormState extends State<BasicInfoForm> {
  String _firstName;
  String _lastName;
  String _middleName;
  String _sex;
  Timestamp _dateOfBirth;
  String _countryOfOrigin = 'NG';
  bool _isLoading = false;

  final _formKey = GlobalKey<FormState>();

  List<DropdownMenuItem> _getCountriesFromApi() {
    return [
      DropdownMenuItem(value: 'NG', child: Text('Nigeria')),
      DropdownMenuItem(value: 'US', child: Text('United States')),
      DropdownMenuItem(value: 'UK', child: Text('United Kindom')),
      DropdownMenuItem(value: 'XX', child: Text('Others')),
    ];
  }

  @override
  Widget build(BuildContext context) {
    String _validateName(String name) {
      return name.length > 1 ? null : 'Enter a first name';
    }

    return StreamBuilder<BasicInfo>(
        stream: DbService(widget.uid).basicInfo,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
            BasicInfo info = snapshot.data;
            return _isLoading ? Loading() : ListView(
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          // First Name
                          TextFormField(
                            initialValue: info.firstName ?? null,
                            onChanged: (val) {
                              setState(() {
                                _firstName = val;
                              });
                            },
                            decoration: InputDecoration(
                              hintText: 'Enter your first name',
                            ),
                            validator: (val) {
                              return _validateName(val);
                            },
                          ),
                          SizedBox(height: 20),
                          // Last Name
                          TextFormField(
                            initialValue: info.lastName ?? null,
                            onChanged: (val) {
                              setState(() {
                                _lastName = val;
                              });
                            },
                            decoration: InputDecoration(
                              hintText: 'Enter your last name',
                            ),
                            validator: (val) {
                              return _validateName(val);
                            },
                          ),
                          SizedBox(height: 20),
                          //Middle Name
                          TextFormField(
                            initialValue: info.middleName ?? null,
                            onChanged: (val) {
                              setState(() {
                                _middleName = val;
                              });
                            },
                            decoration: InputDecoration(
                              hintText: 'Enter your middle name',
                            ),
                            validator: (val) {
                              return _validateName(val);
                            },
                          ),

                          //Sex
                          DropdownButtonFormField(
                            hint: Text('Select your sex'),
                            value: _sex ?? info.sex,
                            items: [
                              DropdownMenuItem(
                                value: 'F',
                                child: Text('Female'),
                              ),
                              DropdownMenuItem(
                                value: 'M',
                                child: Text('Male'),
                              ),
                            ],
                            onChanged: (val) {
                              setState(() {
                                _sex = val;
                              });
                            },
                          ),
                          SizedBox(height: 10),
                          InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  info.dateOfBirth != null
                                      ? DateFormat.yMMMMEEEEd()
                                          .format(info.dateOfBirth.toDate())
                                      : _dateOfBirth != null
                                          ? DateFormat.yMMMMEEEEd()
                                              .format(_dateOfBirth.toDate())
                                          : 'Select your date of birth',
                                  style: TextStyle(fontSize: 16),
                                ),
                                IconButton(
                                  icon: Icon(Icons.calendar_today),
                                  iconSize: 30,
                                ),
                              ],
                            ),
                            onTap: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(1960, 01, 01),
                                  maxTime: DateTime(2002, 01, 01),
                                  theme: DatePickerTheme(
                                    headerColor: Colors.grey[900],
                                    backgroundColor: Colors.grey[850],
                                    doneStyle: TextStyle(
                                        color: Colors.amber, fontSize: 16),
                                    cancelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 16),
                                    itemStyle: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ), onConfirm: (date) {
                                setState(() {
                                  _dateOfBirth = Timestamp.fromDate(date);
                                });
                              });
                            },
                          ),
                          Divider(
                            color: Colors.grey,
                            thickness: 1.5,
                          ),
                          DropdownButtonFormField(
                            hint: Text('Select your Country'),
                            value: _countryOfOrigin ?? info.countryOfOrigin,
                            items: _getCountriesFromApi(),
                            onChanged: (val) {
                              setState(() {
                                _countryOfOrigin = val;
                              });
                            },
                          ),
                          SizedBox(height: 50),
                          MaterialButton(
                            minWidth: double.infinity,
                            color: Colors.amber,
                            child: Text('UPDATE BASIC INFORMATION', style: TextStyle(color: Colors.black),),
                            onPressed: () async {
                              if(_formKey.currentState.validate()) {
                                setState(() {
                                  _isLoading = true;
                                });

                                String uid = Provider.of<User>(context, listen: false)
                                    .uid;
                                BasicInfo basicInfo = new BasicInfo(uid);
                                basicInfo.countryOfOrigin = _countryOfOrigin ?? info.countryOfOrigin;
                                basicInfo.firstName = _firstName ?? info.firstName;
                                basicInfo.lastName = _lastName ?? info.lastName;
                                basicInfo.middleName = _middleName ?? info.middleName;
                                basicInfo.sex = _sex ?? info.sex;
                                basicInfo.dateOfBirth = _dateOfBirth ?? info.dateOfBirth;
                                basicInfo.isSaved = true;

                                print(basicInfo.toString());

                                if(profileStatus!= null){
                                  print('updating status');
                                  profileStatus.basicInfo = true;
                                  print(profileStatus.toString());
                                  await DbService(uid).updateUserProfileStatusInfo(profileStatus);
                                }

                                var result = await DbService(uid).updateBasicInfo(basicInfo);
                                if(result != null) {
                                  Alerts().basicAlert(context, 'Update error');
                                  setState(() {
                                    _isLoading = false;
                                  });
                                } else {
                                  Alerts().basicAlert(context, 'Updated');
                                  Navigator.pop(context);
                                }
                              }


                            },
                          ),
                          SizedBox(height: 50),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          } else if(snapshot.hasError) {
            return ConnectionError();
          } else {
            return Loading();
          }
        });
  }
}
