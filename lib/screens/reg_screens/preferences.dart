import 'package:flutter/material.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class PreferencesForm extends StatefulWidget {
  final String uid;

  PreferencesForm(this.uid);

  @override
  _PreferencesFormState createState() => _PreferencesFormState(uid);
}

class _PreferencesFormState extends State<PreferencesForm> {
  final String uid;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  String _preferredSex;
  String _preferredAge;
  String _preferredEthnicGroup;
  String _preferredReligion;
  bool _smokingTurnOff;
  bool _drinkingTurnOff;
  bool _kidsTurnOff;
  String _dreamPartnerDes;

  _PreferencesFormState(this.uid);

  @override
  Widget build(BuildContext context) {


    return StreamBuilder(
      stream: DbService(uid).preferenceInfo,
      builder: (context, snapshot) {
        if(snapshot.hasError) {
          return ConnectionError();
        } else if(snapshot.hasData) {
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          Preferences preferences = snapshot.data;
          _getReligionOptions(preferences) {
              return [
                DropdownMenuItem(
                  value: 'Christainity',
                  child: Text('Christainity'),
                ),
                DropdownMenuItem(
                  value: 'Islam',
                  child: Text('Islam'),
                ),
                DropdownMenuItem(
                  value: 'None',
                  child: Text('Agnostic()'),
                ),
                DropdownMenuItem(
                  value: 'Others',
                  child: Text('Others'),
                ),
              ];
          }
          return _isLoading ? Loading() : ListView(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          DropdownButtonFormField(
                            value: preferences.preferredSex ??
                                _preferredSex,
                            items: [
                              DropdownMenuItem(
                                value: 'M',
                                child: Text('A man'),
                              ),
                              DropdownMenuItem(
                                value: 'F',
                                child: Text('A woman'),
                              )
                            ],
                            onChanged: (val) {
                              setState(() {
                                _preferredSex = val;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: 'What are you looking for?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.preferredAge ??
                                _preferredAge,
                            items: [
                              DropdownMenuItem(
                                value: '24 - 29',
                                child: Text('25 - 30 years old'),
                              ),
                              DropdownMenuItem(
                                value: '30 - 34',
                                child: Text('30 - 34 years old'),
                              ),
                              DropdownMenuItem(
                                value: '35 - 39',
                                child: Text('35 - 39 years old'),
                              ),
                              DropdownMenuItem(
                                value: '40 - 44',
                                child: Text('40 - 44 years old'),
                              ),
                              DropdownMenuItem(
                                value: '45 - 50',
                                child: Text('45 - 50 years old'),
                              ),
                              DropdownMenuItem(
                                value: '51 - 60',
                                child: Text('51 - 60 years old'),
                              ),
                              DropdownMenuItem(
                                value: '60+',
                                child: Text('Above 60 years old'),
                              ),

                            ],
                            onChanged: (val) {
                              setState(() {
                                _preferredAge = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Preferred partner\'s age?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.preferredEthnicGroup ??
                                _preferredEthnicGroup,
                            items: [
                              DropdownMenuItem(
                                value: 'Hausa',
                                child: Text('Hausa'),
                              ),
                              DropdownMenuItem(
                                value: 'Igbo',
                                child: Text('Igbo'),
                              ),
                              DropdownMenuItem(
                                value: 'Yoruba',
                                child: Text('Yoruba'),
                              ),
                              DropdownMenuItem(
                                value: 'Others',
                                child: Text('Others'),
                              ),
                              DropdownMenuItem(
                                value: 'Doesn\'t Matter',
                                child: Text('Doesn\'t Matter'),
                              ),

                            ],
                            onChanged: (val) {
                              setState(() {
                                _preferredEthnicGroup = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Preferred partner\'s ethnic group?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.preferredReligion ??
                                _preferredReligion,
                            items: _getReligionOptions(preferences),
                            onChanged: (val) {
                              setState(() {
                                _preferredReligion = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Preferred partner\'s religion?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.smokingTurnOff ??
                                _smokingTurnOff,
                            items: [
                              DropdownMenuItem(
                                value: true,
                                child: Text('No'),
                              ),
                              DropdownMenuItem(
                                value: false,
                                child: Text('Yes'),
                              ),
                              DropdownMenuItem(
                                value: null,
                                child: Text('It depends'),
                              ),


                            ],
                            onChanged: (val) {
                              setState(() {
                                _smokingTurnOff = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'A partner that smokes?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.drinkingTurnOff ??
                                _drinkingTurnOff,
                            items: [
                              DropdownMenuItem(
                                value: true,
                                child: Text('No'),
                              ),
                              DropdownMenuItem(
                                value: false,
                                child: Text('Yes'),
                              ),
                              DropdownMenuItem(
                                value: null,
                                child: Text('It depends'),
                              ),


                            ],
                            onChanged: (val) {
                              setState(() {
                                _drinkingTurnOff = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'A partner that smokes?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: preferences.kidsTurnOff ??
                                _kidsTurnOff,
                            items: [
                              DropdownMenuItem(
                                value: true,
                                child: Text('No'),
                              ),
                              DropdownMenuItem(
                                value: false,
                                child: Text('Yes'),
                              ),
                              DropdownMenuItem(
                                value: null,
                                child: Text('It depends'),
                              ),


                            ],
                            onChanged: (val) {
                              setState(() {
                                _kidsTurnOff = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'A partner with kid(s)?'
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            maxLines: 5,
                            initialValue: preferences.dreamPartnerDes ?? null,
                            onChanged: (val){
                              setState(() {
                                _dreamPartnerDes = val;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: 'About preferred partner',
                              hintText: 'A bit about your dream partner',
                            ),
                            validator: (val){
                              if(val.length < 1) return 'Write something';
                              if(val.length > 1 && val.length < 15) return 'Write more about your preferred partner';
                              return null;
                            },
                            keyboardType: TextInputType.multiline,
                          ),


                          SizedBox(height: 50),
                          MaterialButton(
                            minWidth: double.infinity,
                            color: Colors.amber,
                            child: Text(
                              'UPDATE PREFERENCES INFORMATION',
                              style: TextStyle(color: Colors.black),
                            ),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  _isLoading = true;
                                });
                                preferences.preferredSex = _preferredSex ?? preferences.preferredSex;
                                preferences.preferredAge = _preferredAge ?? preferences.preferredAge;
                                preferences.preferredEthnicGroup = _preferredEthnicGroup ?? preferences.preferredEthnicGroup;
                                preferences.preferredReligion = _preferredReligion ?? preferences.preferredReligion;
                                preferences.smokingTurnOff = _smokingTurnOff ?? preferences.smokingTurnOff;
                                preferences.drinkingTurnOff = _drinkingTurnOff ?? preferences.drinkingTurnOff;
                                preferences.kidsTurnOff = _kidsTurnOff ?? preferences.kidsTurnOff;
                                preferences.dreamPartnerDes = _dreamPartnerDes ?? preferences.dreamPartnerDes;
                                preferences.isSaved = true;

                                print(preferences.toString());
                                dynamic result =
                                await DbService(uid)
                                    .updatePreferencesInfo(
                                    preferences);

                                if(profileStatus!= null){
                                  profileStatus.preferences = true;
                                  await DbService(uid).updateUserProfileStatusInfo(profileStatus);
                                }

                                if (result != null) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                } else {
                                  Navigator.pop(context);
                                }
                              }
                            },
                          ),
                          SizedBox(height: 50),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        } else {
          return Loading();
        }
      },
    );
  }
}
