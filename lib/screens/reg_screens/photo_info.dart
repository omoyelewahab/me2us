import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:me2us/models/photos.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/services/file_management_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';

class PhotoInfoForm extends StatefulWidget {
  final String uid;

  PhotoInfoForm(this.uid);

  @override
  _PhotoInfoFormState createState() => _PhotoInfoFormState();
}

class _PhotoInfoFormState extends State<PhotoInfoForm> {
  File _profileImage;
  File _image1;
  File _image2;
  File _image3;

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    _alert(String message) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('$message', style: TextStyle(color: Colors.grey),),
        backgroundColor: Colors.grey[800],
      ));
    }

    Future _getProfileImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if(image != null){
        setState(() {
          _profileImage = image;
          _alert('Image selected. Press save to update');
        });
      }
    }

    Future _getImage1() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if(image!= null){
        setState(() {
          _image1 = image;
          _alert('Image selected. Press save to update');
        });
      }

    }

    Future _getImage2() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if(image!= null){
        setState(() {
          _image2 = image;
          _alert('Image selected. Press save to update');
        });
      }

    }

    Future _getImage3() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if(image!= null){
        setState(() {
          _image3 = image;
          _alert('Image selected. Press save to update');
        });
      }

    }

    _pushAssetImageView(String imgPath){
      MaterialPageRoute route = MaterialPageRoute(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Photo View'),
          ),
          body: Container(
            child: PhotoView(imageProvider: AssetImage('$imgPath'),),
          ),
        );
      });
      Navigator.push(context, route);
    }

    _pushNetworkImageView(String url){
      MaterialPageRoute route = MaterialPageRoute(builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Photo View'),
          ),
          body: Container(
            child: PhotoView(imageProvider: NetworkImage('$url'),),
          ),
        );
      });
      Navigator.push(context, route);
    }

    User user = Provider.of<User>(context);
    return StreamBuilder(
      stream: DbService(user.uid).photosInfo,
      builder: ((context, snapshot) {
        if (snapshot.hasData) {
          PhotosInfo photoInfo = snapshot.data;
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);

          print(photoInfo.otherPhotoPaths.toString());
          final FileServices fileServices = new FileServices(user.uid);
          return isLoading
              ? Loading()
              : Container(
                  child: ListView(children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: CircleAvatar(
                                  radius: 100,
                                  backgroundColor: Colors.amber,
                                  child: InkWell(
                                    child: ClipOval(
                                      child: SizedBox(
                                        width: 190,
                                        height: 190,
                                          child: (_profileImage != null) ?
                                          Image.file(_profileImage, fit: BoxFit.cover,) : (photoInfo.profilePhotoPath != null) ?
                                          Image.network('${photoInfo.profilePhotoPath}', fit: BoxFit.cover,) : Image.network('https://via.placeholder.com/100')
                                      ),
                                    ),
                                    onTap: (){
                                      if(_profileImage != null){
                                        _pushAssetImageView(_profileImage.path);
                                        return;
                                      } else if(photoInfo.profilePhotoPath != null) {
                                        _pushNetworkImageView(photoInfo.profilePhotoPath);
                                        return;
                                      }
                                      _alert('No profile photo is set');

                                    },
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 80),
                                child: IconButton(
                                  icon: Icon(Icons.edit),
                                  iconSize: 30,
                                  onPressed: () {
                                    _getProfileImage();
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 5,
                      thickness: 1,
                    ),
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.amber,
                        radius: 20,
                        child: InkWell(
                          child: ClipOval(
                            child: SizedBox(
                              width: 37,
                              height: 37,
                              child: (_image1 != null) ?
                              Image.file(_image1, fit: BoxFit.cover,) : (photoInfo.otherPhotoPaths != null) ?
                              (photoInfo.otherPhotoPaths[0] != null) ? Image.network(photoInfo.otherPhotoPaths[0]) : Image.network('https://via.placeholder.com/100') :
                              Image.network('https://via.placeholder.com/100')

                            ),
                          ),
                          onTap: (){
                            if(_image1 != null){
                              _pushAssetImageView(_image1.path);
                              return;
                            }else if(photoInfo.otherPhotoPaths!= null && photoInfo.otherPhotoPaths[0]!=null){
                              _pushNetworkImageView(photoInfo.otherPhotoPaths[0]);
                              return;
                            }else{
                              _alert('Photo 1 not set');
                            }
                          },
                        ),
                      ),
                      title: Text(
                        'Photo Upload',
                        style: TextStyle(fontSize: 18),
                      ),
                      subtitle: Text('Tap the image icon to view',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              color: Colors.amber)),
                      trailing: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _getImage1();
                        },
                      ),
                    ),
                    Divider(
                      height: 5,
                      thickness: 1,
                    ),
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.amber,
                        radius: 20,
                        child: InkWell(
                          child: ClipOval(
                            child: SizedBox(
                              width: 37,
                              height: 37,
                                child: (_image2 != null) ?
                                Image.file(_image2, fit: BoxFit.cover,) : (photoInfo.otherPhotoPaths != null) ?
                                (photoInfo.otherPhotoPaths[1] != null) ? Image.network(photoInfo.otherPhotoPaths[1], fit: BoxFit.cover) : Image.network('https://via.placeholder.com/100') :
                                Image.network('https://via.placeholder.com/100')
                            ),
                          ),
                          onTap: (){
                            if(_image2 != null){
                              _pushAssetImageView(_image2.path);
                              return;
                            }else if(photoInfo.otherPhotoPaths!= null && photoInfo.otherPhotoPaths[1]!=null){
                              _pushNetworkImageView(photoInfo.otherPhotoPaths[1]);
                              return;
                            }else{
                              _alert('Photo 2 not set');
                            }
                          },
                        ),
                      ),
                      title: Text(
                        'Photo Upload',
                        style: TextStyle(fontSize: 18),
                      ),
                      subtitle: Text('Tap the image icon to view',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              color: Colors.amber)),
                      trailing: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _getImage2();
                        },
                      ),
                    ),
                    Divider(
                      height: 5,
                      thickness: 1,
                    ),
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.amber,
                        radius: 20,
                        child: InkWell(
                          child: ClipOval(
                            child: SizedBox(
                              width: 37,
                              height: 37,
                                child: (_image3 != null) ?
                                Image.file(_image3, fit: BoxFit.cover,) : (photoInfo.otherPhotoPaths != null) ?
                                (photoInfo.otherPhotoPaths[2] != null) ? Image.network(photoInfo.otherPhotoPaths[2], fit: BoxFit.cover) : Image.network('https://via.placeholder.com/100') :
                                Image.network('https://via.placeholder.com/100')
                            ),
                          ),
                          onTap: (){
                            if(_image3 != null){
                              _pushAssetImageView(_image3.path);
                              return;
                            }else if(photoInfo.otherPhotoPaths!= null && photoInfo.otherPhotoPaths[2]!=null){
                              _pushNetworkImageView(photoInfo.otherPhotoPaths[2]);
                              return;
                            }else{
                              _alert('Photo 3 not set');
                            }
                          },
                        ),
                      ),
                      title: Text(
                        'Photo Upload',
                        style: TextStyle(fontSize: 18),
                      ),
                      subtitle: Text('Tap the image icon to view',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              color: Colors.amber)),
                      trailing: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _getImage3();
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15,15,15,40),
                      child: MaterialButton(
                        minWidth: double.infinity,
                        color: Colors.amber,
                        child: Text(
                          'SAVE PHOTOS',
                          style: TextStyle(color: Colors.black),
                        ),
                        onPressed: () async {
                          if (_profileImage == null &&
                              _image1 == null &&
                              _image2 == null &&
                              _image3 == null) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('No Photo Selected'),
                            ));
                            return;
                          }

                          setState(() {
                            isLoading = true;
                          });

                          if(photoInfo.otherPhotoPaths == null){
                            photoInfo.otherPhotoPaths = [null,null,null];
                          }

                          if (_profileImage != null) {
                            var uploadResult = await fileServices.uploadImage(
                                _profileImage, context);
                            if (photoInfo.profilePhotoPath != null &&
                                photoInfo.profilePhotoPath.length > 10) {
                              await fileServices
                                  .deleteImage(photoInfo.profilePhotoPath);
                            }
                            if (uploadResult != null) {
                              print('Profile Photo uploaded successfully');
                              print(uploadResult);
                              photoInfo.profilePhotoPath = uploadResult;
                            }
                          }

                          if (_image1 != null) {
                            var uploadResult = await fileServices.uploadImage(
                                _image1, context);
                            if (photoInfo.otherPhotoPaths != null) {
                              var paths = photoInfo.otherPhotoPaths;
                              if (paths[0] != null) {
                                fileServices.deleteImage(paths[0]);
                              }
                            }
                            if (uploadResult != null) {
                              print('Photo 1 uploaded successfully');
                              photoInfo.otherPhotoPaths[0] = uploadResult;
                            }
                          }

                          if (_image2 != null) {
                            var uploadResult = await fileServices.uploadImage(
                                _image2, context);
                            if (photoInfo.otherPhotoPaths != null) {
                              var paths = photoInfo.otherPhotoPaths;
                              if (paths[1] != null) {
                                fileServices.deleteImage(paths[1]);
                              }
                            }
                            if (uploadResult != null) {
                              print('Photo 2 uploaded successfully');
                              photoInfo.otherPhotoPaths[1] = uploadResult;
                            }
                          }

                          if (_image3 != null) {
                            var uploadResult = await fileServices.uploadImage(
                                _image3, context);
                            if (photoInfo.otherPhotoPaths != null) {
                              var paths = photoInfo.otherPhotoPaths;
                              if (paths[2] != null) {
                                fileServices.deleteImage(paths[2]);
                              }
                            }
                            if (uploadResult != null) {
                              print('Photo 3 uploaded successfully');
                              photoInfo.otherPhotoPaths[2] = uploadResult;
                            }
                          }

                          photoInfo.isSaved = true;
                          var result = await DbService(user.uid)
                              .updatePhotoInfo(photoInfo);

                          if(profileStatus!= null){
                            profileStatus.photoInfo = true;
                            await DbService(user.uid).updateUserProfileStatusInfo(profileStatus);
                          }

                          if (result != null) {
                            setState(() {
                              isLoading = false;
                            });
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      ),
                    ),
                  ]),
                );
        } else if (snapshot.hasError) {
          return ConnectionError();
        } else {
          return Loading();
        }
      }),
    );
  }
}
