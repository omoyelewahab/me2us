import 'package:flutter/material.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class MaritalInfoForm extends StatefulWidget {
  final String uid;

  MaritalInfoForm(this.uid);

  @override
  _MaritalInfoFormState createState() => _MaritalInfoFormState();
}

class _MaritalInfoFormState extends State<MaritalInfoForm> {
  String _maritalStatus;
  int _numberOfKids;
  int _numberOfWives;
  bool _isSaved;

  List<DropdownMenuItem> _getMaritalStatusOptions() {
    return [
      DropdownMenuItem(value: 'Single', child: Text('Single')),
      DropdownMenuItem(value: 'Married', child: Text('Married')),
      DropdownMenuItem(value: 'Divorced', child: Text('Divorced')),
      DropdownMenuItem(value: 'Widowed', child: Text('Widowed')),
      DropdownMenuItem(value: 'Separated', child: Text('Separated')),
    ];
  }

  List<DropdownMenuItem> _getNumberOfKidsOptions() {
    return [
      DropdownMenuItem(value: 1, child: Text('1 Kid')),
      DropdownMenuItem(value: 2, child: Text('2 Kids')),
      DropdownMenuItem(value: 3, child: Text('3 Kids')),
      DropdownMenuItem(value: 4, child: Text('4 Kids')),
      DropdownMenuItem(value: 5, child: Text('5 Kids')),
      DropdownMenuItem(value: 6, child: Text('More than 5 Kids')),
    ];
  }

  bool showLoading = false;

  String _validateNumber(String email) {
    if (email.length < 1 || email.length > 2) return 'Enter a valid number';
    int number;
    try {
      number = int.parse(email);
    } on Exception catch (e) {
      // TODO
    }
    if (number == null) {
      return 'Enter a valid number';
    } else
      return null;
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<User>(context);

    return StreamBuilder<MaritalInfo>(
      stream: DbService(user.uid).maritalInfo,
      builder: ((context, s) {
        if (s.hasData) {
          var maritalInfo = s.data;
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          return StreamBuilder<BasicInfo>(
            stream: DbService(user.uid).basicInfo,
            builder: ((context, s1) {
              if (s1.hasData) {
                var basicInfo = s1.data;
                return showLoading
                    ? Loading()
                    : ListView(
                        children: <Widget>[
                          Container(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  children: <Widget>[
                                    // Email
                                    DropdownButtonFormField(
                                      hint: Text('Select your marital status'),
                                      value: maritalInfo.maritalStatus ??
                                          _maritalStatus,
                                      items: _getMaritalStatusOptions(),
                                      onChanged: (val) {
                                        setState(() {
                                          _maritalStatus = val;
                                        });
                                      },
                                    ),
                                    SizedBox(height: 20),
                                    // Phone Number
                                    DropdownButtonFormField(
                                      hint: Text('Number of Kids'),
                                      value: maritalInfo.numberOfKids ??
                                          _numberOfKids,
                                      items: _getNumberOfKidsOptions(),
                                      onChanged: (val) {
                                        setState(() {
                                          _numberOfKids = val;
                                        });
                                      },
                                    ),
                                    basicInfo.sex == 'M' &&
                                            _maritalStatus == 'Married'
                                        ? TextFormField(
                                      keyboardType: TextInputType.number,
                                            initialValue: maritalInfo.numberOfWives == null ?
                                                '' : '${maritalInfo.numberOfWives}',
                                            onChanged: (val) {
                                              setState(() {
                                                _numberOfWives =
                                                    int.parse(val.trim());
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: 'Number of Wives',
                                              hintText: 'Enter a number',
                                            ),
                                            validator: (val) {
                                              return _validateNumber(
                                                  val.trim());
                                            },
                                          )
                                        : Container(),
                                    SizedBox(height: 20),

                                    SizedBox(height: 50),
                                    MaterialButton(
                                      minWidth: double.infinity,
                                      color: Colors.amber,
                                      child: Text(
                                        'UPDATE MARITAL INFORMATION',
                                        style: TextStyle(color: Colors.black),
                                      ),
                                      onPressed: () async {
                                        if (_formKey.currentState.validate()) {
                                          setState(() {
                                            showLoading = true;
                                          });
                                          var newMaritalInfo =
                                              MaritalInfo(user.uid);
                                          newMaritalInfo.maritalStatus =
                                              _maritalStatus;
                                          newMaritalInfo.withKids =
                                              _numberOfKids != 0;
                                          newMaritalInfo.numberOfKids =
                                              _numberOfKids;
                                          newMaritalInfo.numberOfWives =
                                              _numberOfKids;
                                          newMaritalInfo.isSaved = true;
                                          dynamic result =
                                              await DbService(user.uid)
                                                  .updateMaritalInfo(
                                                      newMaritalInfo);
                                          if(profileStatus!= null){
                                            profileStatus.maritalInfo = true;
                                            await DbService(user.uid).updateUserProfileStatusInfo(profileStatus);
                                          }
                                          if (result != null) {
                                            setState(() {
                                              showLoading = false;
                                            });
                                          } else {
                                            Navigator.pop(context);
                                          }
                                        }
                                      },
                                    ),
                                    SizedBox(height: 50),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      );
              } else if (s1.hasError) {
                return ConnectionError();
              } else {
                return Loading();
              }
            }),
          );
        } else if (s.hasError) {
          return ConnectionError();
        } else {
          return Loading();
        }
      }),
    );
  }
}
