import 'package:flutter/material.dart';
import 'package:me2us/models/social_media_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class SocialMediaInfoForm extends StatefulWidget {
  final String uid;

  SocialMediaInfoForm(this.uid);

  @override
  _SocialMediaInfoFormState createState() => _SocialMediaInfoFormState();
}

class _SocialMediaInfoFormState extends State<SocialMediaInfoForm> {

  bool _showLoading = false;
  final _formKey = GlobalKey<FormState>();

  String _facebookId;
  String _twitterId;
  String _instagramId;

  String _validateFacebook(SocialMediaInfo info, String val) {
    if((val != null && val.length > 0) && val.length < 3) {
        return 'Invalid facebook ID';
      }
    if((info.twitterId == null && info.instagramId == null) || (info.twitterId.length < 0 && info.instagramId.length < 0) ){
      if((_twitterId == null && _instagramId == null) || (_twitterId.length < 0 && _instagramId.length < 0)) {
        return 'You must supply a social media account username';
      }
    }
    return null;
  }

  String _validateTwitter(SocialMediaInfo info, String val) {
    if((val != null && val.length > 0) && val.length < 3) {
      return 'Invalid twitter ID';
    }
    if((info.facebookId == null && info.instagramId == null) || (info.facebookId.length < 0 && info.instagramId.length < 0) ){
      if((_facebookId == null && _instagramId == null) || (_facebookId.length < 0 && _instagramId.length < 0)) {
        return 'You must supply a social media account username';
      }
    }
    return null;
  }

  String _validateInstagram(SocialMediaInfo info, String val) {
    if((val != null && val.length > 0) && val.length < 3) {
      return 'Invalid instagram ID';
    }
    if((info.facebookId == null && info.twitterId == null) || (info.facebookId.length < 0 && info.twitterId.length < 0) ){
      if((_facebookId == null && _twitterId == null) || (_facebookId.length < 0 && _twitterId.length < 0)) {
        return 'You must supply a social media account username';
      }
    }
    return null;
  }


  String _socialMediaValidation(SocialMediaInfo info, String val) {
    if(val.length < 3 && val != null) {
      if(_facebookId.length < 3 && _instagramId.length < 3 && _twitterId.length < 3){
        return 'Enter a valid username';
      }
    };
    if(info.twitterId == null && info.facebookId == null && info.instagramId == null){
      if(_facebookId == null && _twitterId == null && _instagramId == null) {
        return 'You must supply a social media account username';
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return StreamBuilder(
      stream: DbService(user.uid).socialMediaInfo,
      builder: (context, snapshot){
        if (snapshot.hasData) {
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          SocialMediaInfo socialMediaInfo = snapshot.data;
          return _showLoading ? Loading() : ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          initialValue: socialMediaInfo.facebookId ?? null,
                          onChanged: (val) {
                            setState(() {
                              _facebookId = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Facebook username',
                            hintText: 'Enter your facebook username',
                          ),
                          validator: (val) {
                            return _validateFacebook(socialMediaInfo, val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: socialMediaInfo.twitterId ?? null,
                          onChanged: (val) {
                            setState(() {
                              _twitterId = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Twitter handle',
                            hintText: 'Enter your twitter handle',
                          ),
                          validator: (val) {
                            return _validateTwitter(socialMediaInfo, val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          initialValue: socialMediaInfo.instagramId ?? null,
                          onChanged: (val) {
                            setState(() {
                              _instagramId = val.trim();
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Instagram handle',
                            hintText: 'Enter your instagram handle',
                          ),
                          validator: (val) {
                            return _validateInstagram(socialMediaInfo, val.trim());
                          },
                        ),
                        SizedBox(height: 20),
                        SizedBox(height: 50),
                        MaterialButton(
                          minWidth: double.infinity,
                          color: Colors.amber,
                          child: Text(
                            'UPDATE SOCIAL MEDIA INFORMATION',
                            style: TextStyle(color: Colors.black),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                _showLoading = true;
                              });
                              var newSocialMediaInfo =
                              SocialMediaInfo(user.uid);
                              newSocialMediaInfo.instagramId = _instagramId ?? socialMediaInfo.instagramId;
                              newSocialMediaInfo.facebookId = _facebookId ?? socialMediaInfo.facebookId;
                              newSocialMediaInfo.twitterId = _twitterId ?? socialMediaInfo.twitterId;
                              newSocialMediaInfo.isSaved = true;
                              dynamic result =
                              await DbService(user.uid)
                                  .updateSocialMediaInfo(
                                  newSocialMediaInfo);

                              if(profileStatus!= null){
                                profileStatus.socialMediaInfo = true;
                                await DbService(user.uid).updateUserProfileStatusInfo(profileStatus);
                              }

                              if (result != null) {
                                print('Request done');
                                print(result.toString());
                                setState(() {
                                  _showLoading = false;
                                });
                              } else {
                                Navigator.pop(context);
                              }
                            }
                          },
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        } else if (snapshot.hasError) {
          return ConnectionError();
        } else {
          return Loading();
        }
      },
    );
  }
}
