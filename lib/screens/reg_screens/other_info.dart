import 'package:flutter/material.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class OtherInfoForm extends StatefulWidget {
  final String uid;


  OtherInfoForm(this.uid);

  @override
  _OtherInfoFormState createState() => _OtherInfoFormState(uid);
}

class _OtherInfoFormState extends State<OtherInfoForm> {
  final String uid;

  _OtherInfoFormState(this.uid);
  bool isLoading = false;
  String _briefInfo;
  String _badHabits;
  String _aboutLastRelationship;
  String _inNext3Years;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DbService(uid).otherInfo,
      builder: (context, snapshot) {
        if(snapshot.hasError){
          return ConnectionError();
        }else if(snapshot.hasData) {
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          OtherInfo otherInfo = snapshot.data;
          return isLoading ? Loading() : ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[

                        TextFormField(
                          maxLines: 5,
                          initialValue: otherInfo.briefInfo ?? null,
                          onChanged: (val){
                            setState(() {
                              _briefInfo = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'About me',
                            hintText: 'Write a bit about yourself',
                          ),
                          validator: (val){
                            if(val.length < 1) return 'Enter a short brief';
                            if(val.length > 1 && val.length < 15) return 'Write more about yourself';
                            return null;
                          },
                          keyboardType: TextInputType.multiline,
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          maxLines: 5,
                          initialValue: otherInfo.badHabits ?? null,
                          onChanged: (val){
                            setState(() {
                              _badHabits = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'My bad habits',
                            hintText: 'Write about your bad habits',
                          ),
                          validator: (val){
                            if(val.length < 1) return 'Write about your bad habits';
                            if(val.length > 1 && val.length < 15) return 'Write more about your bad habits';
                            return null;
                          },
                          keyboardType: TextInputType.multiline,
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          maxLines: 5,
                          initialValue: otherInfo.aboutLastRelationship ?? null,
                          onChanged: (val){
                            setState(() {
                              _aboutLastRelationship = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'About my last relationship',
                            hintText: 'Write a bit about your last relationship',
                          ),
                          validator: (val){
                            if(val.length < 1) return 'Write about your last relationship';
                            if(val.length > 1 && val.length < 15) return 'Write more about your last relationship';
                            return null;
                          },
                          keyboardType: TextInputType.multiline,
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          maxLines: 5,
                          initialValue: otherInfo.inNext3Years ?? null,
                          onChanged: (val){
                            setState(() {
                              _inNext3Years = val;
                            });
                          },
                          decoration: InputDecoration(
                            labelText: 'Where I want to be in 3 years',
                            hintText: 'Write a bit about where you like to be in 3 years from now',
                          ),
                          validator: (val){
                            if(val.length < 1) return 'Write about where you want to be in 3 years';
                            if(val.length > 1 && val.length < 15) return 'Write more about where you want to be in 3 years';
                            return null;
                          },
                          keyboardType: TextInputType.multiline,
                        ),
                        SizedBox(height: 20),
                        // Phone Number

                        SizedBox(height: 50),
                        MaterialButton(
                          minWidth: double.infinity,
                          color: Colors.amber,
                          child: Text(
                            'UPDATE OTHER INFORMATION',
                            style: TextStyle(color: Colors.black),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                isLoading = true;
                              });
                              otherInfo.badHabits = _badHabits ?? otherInfo.badHabits;
                              otherInfo.inNext3Years = _inNext3Years ?? otherInfo.inNext3Years;
                              otherInfo.aboutLastRelationship = _aboutLastRelationship ?? otherInfo.aboutLastRelationship;
                              otherInfo.briefInfo = _briefInfo ?? otherInfo.briefInfo;
                              otherInfo.isSaved = true;
                              dynamic result =
                              await DbService(uid)
                                  .updateOtherInfo(
                                  otherInfo);

                              if(profileStatus!= null){
                                profileStatus.otherInfo = true;
                                await DbService(uid).updateUserProfileStatusInfo(profileStatus);
                              }

                              if (result != null) {
                                setState(() {
                                  isLoading = false;
                                });
                              } else {
                                Navigator.pop(context);
                              }
                            }
                          },
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
              )
            ],
          );
        }else{
          return Loading();
        }
      });
  }
}
