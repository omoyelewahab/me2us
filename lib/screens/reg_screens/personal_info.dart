import 'package:flutter/material.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/connection_error.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class PersonalInfoForm extends StatefulWidget {
  final String uid;

  PersonalInfoForm(this.uid);

  @override
  _PersonalInfoFormState createState() => _PersonalInfoFormState(uid);
}

class _PersonalInfoFormState extends State<PersonalInfoForm> {
  final String uid;
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  String _genotype;
  String _ethnicGroup;
  String _religion;
  bool _isPartnerReligionImportant;
  bool _isPartnerEthnicityImportant;
  String _howOftenYouDrink;
  String _howOftenYouSmoke;
  String _hobbies;

  _PersonalInfoFormState(this.uid);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: DbService(uid).personalInfo,
      builder: ((context, snapshot) {
        if(snapshot.hasData){
          UserProfileStatus profileStatus = Provider.of<UserProfileStatus>(context);
          PersonalInfo personalInfo = snapshot.data;
          return _isLoading ? Loading() : ListView(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          DropdownButtonFormField(
                            value: _genotype ??
                                personalInfo.genotype,
                            items: [
                              DropdownMenuItem(
                                value: 'AA',
                                child: Text('AA'),
                              ),
                              DropdownMenuItem(
                                value: 'AS',
                                child: Text('AS'),
                              ),
                              DropdownMenuItem(
                                value: 'AC',
                                child: Text('AC'),
                              ),
                              DropdownMenuItem(
                                value: 'SS',
                                child: Text('SS'),
                              ),
                              DropdownMenuItem(
                                value: 'SC',
                                child: Text('SC'),
                              ),
                              DropdownMenuItem(
                                value: 'CC',
                                child: Text('CC'),
                              ),
                            ],
                            onChanged: (val) {
                              setState(() {
                                _genotype = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'What is your genotype?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _ethnicGroup ??
                                personalInfo.ethnicGroup,
                            items: [
                              DropdownMenuItem(
                                value: 'Hausa',
                                child: Text('Hausa'),
                              ),
                              DropdownMenuItem(
                                value: 'Igbo',
                                child: Text('Igbo'),
                              ),
                              DropdownMenuItem(
                                value: 'Yoruba',
                                child: Text('Yoruba'),
                              ),
                              DropdownMenuItem(
                                value: 'Others',
                                child: Text('Others'),
                              ),

                            ],
                            onChanged: (val) {
                              setState(() {
                                _ethnicGroup = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Your ethnic group'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _religion ??
                                personalInfo.religion,
                            items: [
                              DropdownMenuItem(
                                value: 'Christainity',
                                child: Text('Christainity'),
                              ),
                              DropdownMenuItem(
                                value: 'Islam',
                                child: Text('Islam'),
                              ),
                              DropdownMenuItem(
                                value: 'Others',
                                child: Text('Others'),
                              ),

                            ],
                            onChanged: (val) {
                              setState(() {
                                _religion = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Your religion'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _isPartnerReligionImportant ??
                                personalInfo.isPartnerReligionImportant,
                            items: [
                              DropdownMenuItem(
                                value: false,
                                child: Text('No'),
                              ),
                              DropdownMenuItem(
                                value: true,
                                child: Text('Yes'),
                              ),
                            ],
                            onChanged: (val) {
                              setState(() {
                                _isPartnerReligionImportant = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Is your partner\'s religion important to you?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _isPartnerEthnicityImportant ??
                                personalInfo.isPartnerEthnicityImportant,
                            items: [
                              DropdownMenuItem(
                                value: false,
                                child: Text('No'),
                              ),
                              DropdownMenuItem(
                                value: true,
                                child: Text('Yes'),
                              ),
                            ],
                            onChanged: (val) {
                              setState(() {
                                _isPartnerEthnicityImportant = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'Is your partner\'s ethnicity important to you?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _howOftenYouDrink ??
                                personalInfo.howOftenYouDrink,
                            items: [
                              DropdownMenuItem(
                                value: 'Quite often',
                                child: Text('Quite often'),
                              ),
                              DropdownMenuItem(
                                value: 'Occasionally',
                                child: Text('Occasionally'),
                              ),
                              DropdownMenuItem(
                                value: 'Rarely',
                                child: Text('Rarely'),
                              ),
                              DropdownMenuItem(
                                value: 'Never',
                                child: Text('Never'),
                              ),


                            ],
                            onChanged: (val) {
                              setState(() {
                                _howOftenYouDrink = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'How often do you drink?'
                            ),
                          ),
                          SizedBox(height: 20),
                          DropdownButtonFormField(
                            value: _howOftenYouSmoke ??
                                personalInfo.howOftenYouSmoke,
                            items: [
                              DropdownMenuItem(
                                value: 'Quite often',
                                child: Text('Quite often'),
                              ),
                              DropdownMenuItem(
                                value: 'Occasionally',
                                child: Text('Occasionally'),
                              ),
                              DropdownMenuItem(
                                value: 'Rarely',
                                child: Text('Rarely'),
                              ),
                              DropdownMenuItem(
                                value: 'Never',
                                child: Text('Never'),
                              ),


                            ],
                            onChanged: (val) {
                              setState(() {
                                _howOftenYouSmoke = val;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: 'How often do you smoke?'
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            maxLines: 5,
                            initialValue: _hobbies ?? personalInfo.hobbies,
                            onChanged: (val){
                              setState(() {
                                _hobbies = val;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: 'What are your hobbies?',
                            ),
                            validator: (val){
                              if(val.length < 1) return 'Write something';
                              if(val.length > 1 && val.length < 10) return 'Write more about your hobbies';
                              return null;
                            },
                            keyboardType: TextInputType.multiline,
                          ),


                          SizedBox(height: 50),
                          MaterialButton(
                            minWidth: double.infinity,
                            color: Colors.amber,
                            child: Text(
                              'UPDATE PERSONAL INFORMATION',
                              style: TextStyle(color: Colors.black),
                            ),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  _isLoading = true;
                                });
                                personalInfo.genotype = _genotype ?? personalInfo.genotype;
                                personalInfo.religion = _religion ?? personalInfo.religion;
                                personalInfo.ethnicGroup = _ethnicGroup ?? personalInfo.ethnicGroup;
                                personalInfo.isPartnerEthnicityImportant = _isPartnerEthnicityImportant ?? personalInfo.isPartnerEthnicityImportant;
                                personalInfo.isPartnerReligionImportant = _isPartnerReligionImportant ?? personalInfo.isPartnerReligionImportant;
                                personalInfo.howOftenYouSmoke = _howOftenYouSmoke ?? personalInfo.howOftenYouSmoke;
                                personalInfo.howOftenYouDrink = _howOftenYouDrink ?? personalInfo.howOftenYouDrink;
                                personalInfo.hobbies = _hobbies ?? personalInfo.hobbies;
                                personalInfo.isSaved = true;
                                print(personalInfo.toString());

                                print(profileStatus.toString());
                                if(profileStatus!= null){
                                  profileStatus.personalInfo = true;
                                  await DbService(uid).updateUserProfileStatusInfo(profileStatus);
                                }
                                print(profileStatus.toString());
                                dynamic result =
                                await DbService(uid)
                                    .updatePersonalInfo(
                                    personalInfo);
                                if (result != null) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                } else {
                                  Navigator.pop(context);
                                }
                              }
                            },
                          ),
                          SizedBox(height: 50),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        } else if(snapshot.hasError) {
          return ConnectionError();
        } else {
          return Loading();
        }
      }),
    );
  }
}
