import 'package:flutter/material.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/photos.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/social_media_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';
import 'package:me2us/screens/edit_profile.dart';
import 'package:me2us/screens/profile.dart';
import 'package:me2us/services/auth.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class HomeMenu extends StatefulWidget {
  @override
  _HomeMenuState createState() => _HomeMenuState();
}

class _HomeMenuState extends State<HomeMenu> {
  AuthService _authService = new AuthService();
  _pushEditProfilePage(String uid) {
    MaterialPageRoute route = new MaterialPageRoute(builder: (context) {
      return MultiProvider(providers: [
        StreamProvider<User>.value(value: AuthService().user),
        StreamProvider<BasicInfo>.value(value: DbService(uid).basicInfo),
        StreamProvider<ContactInfo>.value(value: DbService(uid).contactInfo),
        StreamProvider<EmploymentInfo>.value(
            value: DbService(uid).employmentInfo),
        StreamProvider<MaritalInfo>.value(value: DbService(uid).maritalInfo),
        StreamProvider<OtherInfo>.value(value: DbService(uid).otherInfo),
        StreamProvider<PersonalInfo>.value(value: DbService(uid).personalInfo),
        StreamProvider<Preferences>.value(value: DbService(uid).preferenceInfo),
        StreamProvider<SocialMediaInfo>.value(
            value: DbService(uid).socialMediaInfo),
        StreamProvider<PhotosInfo>.value(
            value: DbService(uid).photosInfo),
        StreamProvider<UserProfileStatus>.value(
          value: DbService(uid).userProfileStatus,
        )
      ], child: ProfilePage());
    });

    Navigator.of(context).push(route);
  }

  @override
  Widget build(BuildContext context) {
    String uid = Provider.of<User>(context).uid;
    ContactInfo contactInfo = Provider.of<ContactInfo>(context);
    if (contactInfo == null) {
      return Loading();
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            child: Column(
              children: <Widget>[
                InkWell(
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Container(
                      child: Image(
                        image: AssetImage('assets/profile1.png'),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                  ),
                  onTap: () {
                    _pushEditProfilePage(uid);
                  },
                ),
                SizedBox(height: 5),
                InkWell(
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Container(
                      //height: 200,
                      child: Image(
                        image: AssetImage('assets/shop.png'),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                  ),
                  onTap: () {
                    _authService.signOut();
                  },
                ),
                SizedBox(height: 5),
                Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Container(
                    //height: 200,
                    child: Image(
                      image: AssetImage('assets/forum.png'),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
