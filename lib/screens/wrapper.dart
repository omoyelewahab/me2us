import 'package:flutter/material.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/screens/authenticate/authenticate.dart';
import 'package:me2us/screens/home.dart';
import 'package:me2us/screens/home_wrapper.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    User user = Provider.of<User>(context);

    // Return authenticate or home
    if(user == null) {
      return Authenticate();
    }else{
      return HomeWrapper();
    }
  }
}
