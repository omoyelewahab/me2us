import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/screens/home_menu.dart';
import 'package:me2us/services/auth.dart';
import 'package:me2us/services/db_service.dart';
import 'package:me2us/shared/loading.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: HomeMenu(),
      ),
    );
  }
}



//class Home extends StatefulWidget {
//  @override
//  _HomeState createState() => _HomeState();
//}
//
//class _HomeState extends State<Home> {
//
//  void _editProfile() {
//     Navigator.pushNamed(context, '/edit-profile');
//  }
//
//  Future _getProfile() async {
//    String uid = Provider.of<User>(context).uid;
//    // BasicInfo basicInfo = Provider.of<BasicInfo>(context);
//    var data = DbService(uid).basicInfo;
//    print('This is $data');
//  }
//
//  final AuthService _authService = new AuthService();
//
//
//  @override
//  Widget build(BuildContext context) {
//    String uid = Provider.of<User>(context).uid;
//    BasicInfo basicInfo = Provider.of<BasicInfo>(context);
//    Stream<BasicInfo> basicInfoStream = DbService(uid).basicInfo;
//    if(basicInfo != null){
//      return Scaffold(
//        body: Container(
//          decoration: BoxDecoration(
//            image: DecorationImage(
//              image: AssetImage('assets/bbg.png'),
//              fit: BoxFit.cover,
//            ),
//          ),
//
//          child: Column(
//            mainAxisAlignment: MainAxisAlignment.end,
//            children: <Widget>[
//              Padding(
//                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
//                child: Column(
//                  children: <Widget>[
//                    InkWell(
//                      child: Card(
//                        semanticContainer: true,
//                        clipBehavior: Clip.antiAliasWithSaveLayer,
//                        child: Container(
//                          child: Image(
//                            image: AssetImage('assets/profile1.png'),
//                          ),
//
//                        ),
//                        shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(10.0),
//                        ),
//                        elevation: 5,
//                      ),
//                      onTap: (){
//                        Navigator.pushNamed(context, '/edit-profile');
//                      },
//                    ),
//                    SizedBox(height: 5),
//                    InkWell(
//                      child: Card(
//                        semanticContainer: true,
//                        clipBehavior: Clip.antiAliasWithSaveLayer,
//                        child: Container(
//                          //height: 200,
//                          child: Image(
//                            image: AssetImage('assets/shop.png'),
//                          ),
//                        ),
//                        shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(10.0),
//                        ),
//                        elevation: 5,
//                      ),
//                      onTap: () {
//                        // _authService.signOut();
//                      },
//                    ),
//                    SizedBox(height: 5),
//                    Card(
//                      semanticContainer: true,
//                      clipBehavior: Clip.antiAliasWithSaveLayer,
//                      child: Container(
//                        //height: 200,
//                        child: Image(
//                          image: AssetImage('assets/forum.png'),
//                        ),
//                      ),
//                      shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.circular(10.0),
//                      ),
//                      elevation: 5,
//                    ),
//                    Text(basicInfo.uid),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
//      );
//    }else{
//      return Loading();
//    }
//  }
//}

