import 'package:flutter/material.dart';
class Alerts{
   void basicAlert(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('$message', style: TextStyle(color: Colors.grey),),
      backgroundColor: Colors.grey[800],
    ));
  }
}