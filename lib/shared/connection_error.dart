import 'package:flutter/material.dart';

class ConnectionError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Connection Error'),
              Text('Check Your Internet Connection'),
            ]
        ),
      ),
    );
  }
}

class StatusNotCompleted extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Incomplete Profile'),
              Text('Make sure you have submitted all the sections'),
            ]
        ),
      ),
    );
  }
}
