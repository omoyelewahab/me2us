import 'package:firebase_auth/firebase_auth.dart';
import 'package:me2us/models/user.dart';

import 'db_service.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  User _userFromFirebaseAuth(FirebaseUser firebaseUser) {
    var user = new User(uid: firebaseUser.uid);
    user.email = firebaseUser.email;
    return firebaseUser == null ? null : user;
  }

  // Emits user Stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
        .map((FirebaseUser user) => _userFromFirebaseAuth(user));
  }

  //Sign-in anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseAuth(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign in with email
  Future signIn(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFirebaseAuth(user);
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future registerWithEmail(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User user = _userFromFirebaseAuth(result.user);
      await DbService(user.uid).createNewUserInfo();
      return user;
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  // Sign out

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
