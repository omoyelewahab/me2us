import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/photos.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/social_media_info.dart';
import 'package:me2us/models/user.dart';
import 'package:me2us/models/user_profile_status.dart';

class DbService {
  List<String> errors = [];
  final String uid;
  DbService(this.uid);

  final CollectionReference basicInfoCollection =
      Firestore.instance.collection('basic_info');
  final CollectionReference contactInfoCollection =
      Firestore.instance.collection('contact_info');
  final CollectionReference employmentInfoCollection =
      Firestore.instance.collection('employment_info');
  final CollectionReference maritalInfoCollection =
      Firestore.instance.collection('marital_info');
  final CollectionReference otherInfoCollection =
      Firestore.instance.collection('other_info');
  final CollectionReference personalInfoCollection =
      Firestore.instance.collection('personal_info');
  final CollectionReference preferenceInfoCollection =
      Firestore.instance.collection('preference_info');
  final CollectionReference socialMediaInfoCollection =
      Firestore.instance.collection('social_media_info');
  final CollectionReference userProfileStatusCollection =
      Firestore.instance.collection('user_profile_status');
  final CollectionReference photoInfoCollection =
      Firestore.instance.collection('phot_info');

  Future _initiateUserRecords() async {
    BasicInfo basicInfo = new BasicInfo(uid);
    ContactInfo contactInfo = new ContactInfo(uid);
    EmploymentInfo employmentInfo = new EmploymentInfo(uid);
    MaritalInfo maritalInfo = new MaritalInfo(uid);
    OtherInfo otherInfo = new OtherInfo(uid);
    PersonalInfo personalInfo = new PersonalInfo(uid);
    Preferences preferences = new Preferences(uid);
    SocialMediaInfo socialMediaInfo = new SocialMediaInfo(uid);
    UserProfileStatus userProfileStatus = new UserProfileStatus(uid);
    PhotosInfo photosInfo = new PhotosInfo(uid);

    // Using the mapper package (encode) to return map for each object
    try {
      basicInfoCollection.document(uid).setData(basicInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Basic Info record not set - $e');
    }
    try {
      contactInfoCollection.document(uid).setData(contactInfo.toMap());
    } catch (e) {
      errors.add('Error: Contact Info record not set - $e');
    }
    try {
      employmentInfoCollection.document(uid).setData(employmentInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Employment Info record not set - $e');
    }
    try {
      maritalInfoCollection.document(uid).setData(maritalInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Marital Info record not set - $e');
    }
    try {
      otherInfoCollection.document(uid).setData(otherInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Other Info record not set - $e');
    }
    try {
      personalInfoCollection.document(uid).setData(personalInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Personal Info record not set - $e');
    }
    try {
      preferenceInfoCollection.document(uid).setData(preferences.toMap());
    } on Exception catch (e) {
      errors.add('Error: Preference Info record not set - $e');
    }
    try {
      socialMediaInfoCollection.document(uid).setData(socialMediaInfo.toMap());
    } on Exception catch (e) {
      errors.add('Error: Social Media Info record not set - $e');
    }
    try {
      userProfileStatusCollection
          .document(uid)
          .setData(userProfileStatus.toMap());
    } on Exception catch (e) {
      errors.add('Error: Profile status Info record not set - $e');
    }
    try{
      photoInfoCollection.document(uid).setData(photosInfo.toMap());
    }catch (e) {
      errors.add('Error: Photos record not set');
    }
  }

  Future createNewUserInfo() async {
    errors.clear();
    await _initiateUserRecords();
    return errors.length > 0 ? errors : null;
  }

  Stream<BasicInfo> get basicInfo {
    BasicInfo basicInfo = new BasicInfo(uid);
    return basicInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => basicInfo.basicInfoFromSnapshot(snapshot));
  }

  Future<DocumentSnapshot> getBasicInfo() {
    return basicInfoCollection.document(uid).get();
  }

  Future updateBasicInfo(BasicInfo basicInfo) async {
    DocumentReference basicInfoReference = basicInfoCollection.document(uid);
    return await basicInfoReference.setData(basicInfo.toMap());
  }

  Stream<ContactInfo> get contactInfo {
    ContactInfo contactInfo = new ContactInfo(uid);
    return contactInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => contactInfo.contactInfoFromSnapshot(snapshot));
  }

  Future updateContactInfo(ContactInfo contactInfo) async {
    DocumentReference contactInfoReference =
        contactInfoCollection.document(uid);
    return await contactInfoReference.setData(contactInfo.toMap());
  }

  Stream<EmploymentInfo> get employmentInfo {
    EmploymentInfo employmentInfo = new EmploymentInfo(uid);
    return employmentInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => employmentInfo.employmentInfoFromSnapshot(snapshot));
  }

  Future updateEmploymentInfo(EmploymentInfo employmentInfo) async {
    DocumentReference employmentInfoReference =
        employmentInfoCollection.document(uid);
    return await employmentInfoReference.setData(employmentInfo.toMap());
  }

  Stream<MaritalInfo> get maritalInfo {
    MaritalInfo maritalInfo = new MaritalInfo(uid);
    return maritalInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => maritalInfo.maritalInfoFromSnapshot(snapshot));
  }

  Future updateMaritalInfo(MaritalInfo maritalInfo) async {
    DocumentReference maritalInfoReference =
        maritalInfoCollection.document(uid);
    return await maritalInfoReference.setData(maritalInfo.toMap());
  }

  Stream<OtherInfo> get otherInfo {
    OtherInfo otherInfo = new OtherInfo(uid);
    return otherInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => otherInfo.otherInfoFromSnapshot(snapshot));
  }

  Future updateOtherInfo(OtherInfo otherInfo) async {
    DocumentReference otherInfoReference = otherInfoCollection.document(uid);
    return await otherInfoReference.setData(otherInfo.toMap());
  }

  Stream<PersonalInfo> get personalInfo {
    PersonalInfo personalInfo = new PersonalInfo(uid);
    return personalInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => personalInfo.personalInfoFromSnapshot(snapshot));
  }

  Future updatePersonalInfo(PersonalInfo personalInfo) async {
    DocumentReference personalInfoReference =
        personalInfoCollection.document(uid);
    return await personalInfoReference.setData(personalInfo.toMap());
  }

  Stream<Preferences> get preferenceInfo {
    Preferences preferences = new Preferences(uid);
    return preferenceInfoCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => preferences.preferencesFromSnapshot(snapshot));
  }

  Future updatePreferencesInfo(Preferences preferences) async {
    DocumentReference preferencesInfoReference =
        preferenceInfoCollection.document(uid);
    return await preferencesInfoReference.setData(preferences.toMap());
  }

  Stream<SocialMediaInfo> get socialMediaInfo {
    SocialMediaInfo socialMediaInfo = new SocialMediaInfo(uid);
    return socialMediaInfoCollection.document(uid).snapshots().map(
        (snapshot) => socialMediaInfo.socialMediaInfoFromSnapshot(snapshot));
  }

  Future updateSocialMediaInfo(SocialMediaInfo socialMediaInfo) async {
    DocumentReference socialMediaInfoReference =
        socialMediaInfoCollection.document(uid);
    return await socialMediaInfoReference.setData(socialMediaInfo.toMap());
  }

  Stream<UserProfileStatus> get userProfileStatus {
    UserProfileStatus status = new UserProfileStatus(uid);
    return userProfileStatusCollection
        .document(uid)
        .snapshots()
        .map((snapshot) => status.userProfileStatusFromSnapshot(snapshot));
  }

  Future updateUserProfileStatusInfo(
      UserProfileStatus userProfileStatus) async {
    DocumentReference userProfileStatusInfoReference =
        userProfileStatusCollection.document(uid);
    return await userProfileStatusInfoReference
        .setData(userProfileStatus.toMap());
  }

  Stream<PhotosInfo> get photosInfo{
    PhotosInfo photosInfo = new PhotosInfo(uid);
    return photoInfoCollection.document(uid).snapshots().map((snapshot) => photosInfo.photosInfoFromSnapshot(snapshot));
  }

  Future updatePhotoInfo(PhotosInfo photosInfo) async {
    DocumentReference documentReference = photoInfoCollection.document(uid);
    return await documentReference.updateData(photosInfo.toMap());
  }
}
