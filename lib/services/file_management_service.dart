import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';

class FileServices {
  final String uid;

  FileServices(this.uid);

  final FirebaseStorage _storage = FirebaseStorage.instance;

  Future uploadImage(File image, BuildContext context) async {
    try {
      String filePath = basename(image.path) + uid;
      StorageReference fileReference = _storage.ref().child(filePath);
      StorageUploadTask uploadTask = fileReference.putFile(image);

      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
      String url = await taskSnapshot.ref.getDownloadURL();
      print('Image Uploaded at: $url');
      return url;

      // String url = await fileReference.getDownloadURL();
      // print('Image Uploaded!!! Download url is: $url');
      // print('Storage reference is: ${fileReference.toString()}');

    } on Exception catch (e) {
      // TODO
      print('Error uploading image!!');
      return null;
    }
  }

  Future deleteImage(String imageUrl) async {
    StorageReference ref = await _storage.getReferenceFromUrl(imageUrl);
    return _storage.ref().child(ref.path).delete();
  }


}