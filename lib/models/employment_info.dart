import 'package:cloud_firestore/cloud_firestore.dart';

class EmploymentInfo{

  String uid;
  String employmentStatus;
  String occupation;
  String placeOfWork;
  String workAddress1;
  String workAddress2;
  String annualSalaryRange;
  bool isSaved = false;

  EmploymentInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid' : uid,
    'employmentStatus': employmentStatus,
    'occupation': occupation,
    'placeOfWork': placeOfWork,
    'workAddress1': workAddress1,
    'workAddress2': workAddress2,
    'annualSalaryRange': annualSalaryRange,
    'isSaved': isSaved,
    };
  }

  EmploymentInfo employmentInfoFromSnapshot(DocumentSnapshot snapshot){
    EmploymentInfo info = new EmploymentInfo(uid);
    info.employmentStatus = snapshot.data['employmentStatus'];
    info.occupation = snapshot.data['occupation'];
    info.placeOfWork = snapshot.data['placeOfWork'];
    info.workAddress1 = snapshot.data['workAddress1'];
    info.workAddress2 = snapshot.data['workAddress2'];
    info.annualSalaryRange = snapshot.data['annualSalaryRange'];
    info.isSaved = snapshot.data['isSaved'];
    return info;

  }
}