import 'package:cloud_firestore/cloud_firestore.dart';

class Preferences{

  String uid;
  String preferredSex;
  String preferredAge;
  String preferredEthnicGroup;
  String preferredReligion;
  bool smokingTurnOff;
  bool drinkingTurnOff;
  bool kidsTurnOff;
  String dreamPartnerDes;
  bool isSaved = false;

  Preferences(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'preferredSex': preferredSex,
    'preferredAge': preferredAge,
    'preferredEthnicGroup': preferredEthnicGroup,
    'preferredReligion': preferredReligion,
    'smokingTurnOff': smokingTurnOff,
    'drinkingTurnOff': drinkingTurnOff,
    'kidsTurnOff': kidsTurnOff,
    'dreamPartnerDes': dreamPartnerDes,
    'isSaved': isSaved
    };
  }

  Preferences preferencesFromSnapshot (DocumentSnapshot snapshot) {
    Preferences preferences = new Preferences(uid);
    preferences.preferredSex = snapshot.data['preferredSex'];
    preferences.preferredAge = snapshot.data['preferredAge'];
    preferences.preferredEthnicGroup = snapshot.data['preferredEthnicGroup'];
    preferences.preferredReligion = snapshot.data['preferredReligion'];
    preferences.smokingTurnOff = snapshot.data['smokingTurnOff'];
    preferences.drinkingTurnOff = snapshot.data['drinkingTurnOff'];
    preferences.kidsTurnOff = snapshot.data['kidsTurnOff'];
    preferences.dreamPartnerDes = snapshot.data['dreamPartnerDes'];
    preferences.isSaved = snapshot.data['isSaved'];
    return preferences;
  }

  @override
  String toString() {
    return 'Preferences{uid: $uid, preferredSex: $preferredSex, preferredAge: $preferredAge, preferredEthnicGroup: $preferredEthnicGroup, preferredReligion: $preferredReligion, smokingTurnOff: $smokingTurnOff, drinkingTurnOff: $drinkingTurnOff, kidsTurnOff: $kidsTurnOff, dreamPartnerDes: $dreamPartnerDes, isSaved: $isSaved}';
  }


}