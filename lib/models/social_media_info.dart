import 'package:cloud_firestore/cloud_firestore.dart';

class SocialMediaInfo{

  String uid;
  String facebookId;
  String twitterId;
  String instagramId;
  bool isSaved = false;

  SocialMediaInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'facebookId': facebookId,
    'twitterId': twitterId,
    'instagramId': instagramId,
    'isSaved': isSaved
    };
  }

  SocialMediaInfo socialMediaInfoFromSnapshot(DocumentSnapshot snapshot) {
    SocialMediaInfo socialMediaInfo = new SocialMediaInfo(uid);
    socialMediaInfo.facebookId = snapshot.data['facebookId'];
    socialMediaInfo.twitterId = snapshot.data['twitterId'];
    socialMediaInfo.instagramId = snapshot.data['instagramId'];
    socialMediaInfo.isSaved = snapshot.data['isSaved'];
    return socialMediaInfo;
  }
}