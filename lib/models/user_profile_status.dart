
import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfileStatus{
  String uid;
  bool basicInfo = false;
  bool contactInfo = false;
  bool employmentInfo = false;
  bool maritalInfo = false;
  bool otherInfo = false;
  bool personalInfo = false;
  bool preferences = false;
  bool socialMediaInfo = false;
  bool photoInfo;
  bool submitted = false;

  UserProfileStatus(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'basicInfo': basicInfo,
    'contactInfo': contactInfo,
    'employmentInfo': employmentInfo,
    'maritalInfo': maritalInfo,
    'otherInfo': otherInfo,
    'personalInfo': personalInfo,
    'preferences': preferences,
    'socialMediaInfo': socialMediaInfo,
    'submitted': submitted,
      'photoInfo': photoInfo
    };
  }

  UserProfileStatus userProfileStatusFromSnapshot (DocumentSnapshot shot){
    UserProfileStatus status = new UserProfileStatus(uid);
    status.basicInfo = shot.data['basicInfo'];
    status.contactInfo = shot.data['contactInfo'];
    status.employmentInfo = shot.data['employmentInfo'];
    status.maritalInfo = shot.data['maritalInfo'];
    status.otherInfo = shot.data['otherInfo'];
    status.personalInfo = shot.data['personalInfo'];
    status.preferences = shot.data['preferences'];
    status.socialMediaInfo = shot.data['socialMediaInfo'];
    status.submitted = shot.data['submitted'];
    status.photoInfo = shot.data['photoInfo'];
    return status;
  }

  @override
  String toString() {
    return 'UserProfileStatus{uid: $uid, basicInfo: $basicInfo, contactInfo: $contactInfo, employmentInfo: $employmentInfo, maritalInfo: $maritalInfo, otherInfo: $otherInfo, personalInfo: $personalInfo, preferences: $preferences, socialMediaInfo: $socialMediaInfo, photoInfo: $photoInfo, submitted: $submitted}';
  }


}