import 'package:cloud_firestore/cloud_firestore.dart';

class MaritalInfo{

  String uid;
  String maritalStatus;
  bool withKids;
  int numberOfKids;
  int numberOfWives;
  bool isSaved = false;

  MaritalInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'maritalStatus': maritalStatus,
    'withKids': withKids,
    'numberOfKids': numberOfKids,
    'numberOfWives': numberOfWives,
    'isSaved': isSaved,
    };
  }

  MaritalInfo maritalInfoFromSnapshot(DocumentSnapshot snapshot){
    MaritalInfo maritalInfo = new MaritalInfo(uid);
    maritalInfo.maritalStatus = snapshot.data['maritalStatus'];
    maritalInfo.withKids = snapshot.data['withKids'];
    maritalInfo.numberOfWives = snapshot.data['numberOfWives'];
    maritalInfo.numberOfKids = snapshot.data['numberOfKids'];
    maritalInfo.isSaved = snapshot.data['isSaved'];
    return maritalInfo;
  }
}