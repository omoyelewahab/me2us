import 'package:me2us/models/basic_info.dart';
import 'package:me2us/models/contact_info.dart';
import 'package:me2us/models/employment_info.dart';
import 'package:me2us/models/marital_info.dart';
import 'package:me2us/models/other_info.dart';
import 'package:me2us/models/personal_info.dart';
import 'package:me2us/models/preferences_info.dart';
import 'package:me2us/models/social_media_info.dart';

class UserProfile{
  String uid;
  BasicInfo basicInfo;
  ContactInfo contactInfo;
  EmploymentInfo employmentInfo;
  MaritalInfo maritalInfo;
  OtherInfo otherInfo;
  PersonalInfo personalInfo;
  Preferences preferences;
  SocialMediaInfo socialMediaInfo;


  UserProfile(this.uid);
}