import 'package:cloud_firestore/cloud_firestore.dart';

class ContactInfo{

  String uid;
  String email;
  String phoneNumber;
  String address1;
  String address2;
  String countryOfResidence;
  bool isSaved = false;

  ContactInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid' : uid,
    'email' : email,
    'phoneNumber' : phoneNumber,
    'address1': address1,
    'address2' : address2,
    'countryOfResidence' : countryOfResidence,
    'isSaved' : isSaved,
    };
  }

  ContactInfo contactInfoFromSnapshot(DocumentSnapshot snapshot) {
    ContactInfo contactInfo = new ContactInfo(uid);
    contactInfo.email = snapshot.data['email'];
    contactInfo.phoneNumber = snapshot.data['phoneNumber'];
    contactInfo.address1 = snapshot.data['address1'];
    contactInfo.isSaved = snapshot.data['isSaved'];
    contactInfo.address2 = snapshot.data['address2'];
    contactInfo.countryOfResidence = snapshot.data['countryOfResidence'];
    return contactInfo;
  }
}