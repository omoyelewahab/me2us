import 'package:cloud_firestore/cloud_firestore.dart';

class BasicInfo{

  String uid;
  String firstName;
  String lastName;
  String middleName;
  String sex;
  Timestamp dateOfBirth;
  String countryOfOrigin;
  bool isSaved = false;

  BasicInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'firstName': firstName,
      'lastName' : lastName,
      'middleName' : middleName,
      'sex' : sex,
      'dateOfBirth' : dateOfBirth,
      'countryOfOrigin': countryOfOrigin,
      'isSaved' : isSaved,
    };
  }


  @override
  String toString() {
    return 'BasicInfo{uid: $uid, firstName: $firstName, lastName: $lastName, middleName: $middleName, sex: $sex, dateOfBirth: $dateOfBirth, countryOfOrigin: $countryOfOrigin, isSaved: $isSaved}';
  }

  BasicInfo basicInfoFromSnapshot(DocumentSnapshot snapshot) {
    BasicInfo basicInfo = new BasicInfo(uid);
    basicInfo.countryOfOrigin = snapshot.data['countryOfOrigin'];
    basicInfo.dateOfBirth = snapshot.data['dateOfBirth'];
    basicInfo.firstName = snapshot.data['firstName'];
    basicInfo.isSaved = snapshot.data['isSaved'];
    basicInfo.lastName = snapshot.data['lastName'];
    basicInfo.middleName = snapshot.data['middleName'];
    basicInfo.sex = snapshot.data['sex'];
    return basicInfo;
  }
}
