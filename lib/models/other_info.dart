import 'package:cloud_firestore/cloud_firestore.dart';

class OtherInfo{

  String uid;
  String briefInfo;
  String badHabits;
  String aboutLastRelationship;
  String inNext3Years;
  bool isSaved = false;

  OtherInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'briefInfo': briefInfo,
    'badHabits': badHabits,
    'aboutLastRelationship': aboutLastRelationship,
    'inNext3Years': inNext3Years,
    'isSaved': isSaved
    };
  }

  OtherInfo otherInfoFromSnapshot(DocumentSnapshot snapshot){
    OtherInfo otherInfo = new OtherInfo(uid);
    otherInfo.briefInfo = snapshot.data['briefInfo'];
    otherInfo.badHabits = snapshot.data['badHabits'];
    otherInfo.aboutLastRelationship = snapshot.data['aboutLastRelationship'];
    otherInfo.inNext3Years = snapshot.data['inNext3Years'];
    otherInfo.isSaved = snapshot.data['isSaved'];
    return otherInfo;
  }
}