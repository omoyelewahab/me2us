class User{
  final String uid;
  String email;
  String phoneNumber;

  User({this.uid});

  @override
  String toString() {
    return 'User{uid: $uid, email: $email, phoneNumber: $phoneNumber}';
  }

}