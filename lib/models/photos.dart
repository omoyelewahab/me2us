import 'package:cloud_firestore/cloud_firestore.dart';

class PhotosInfo {
  String uid;
  String profilePhotoPath;
  List<dynamic> otherPhotoPaths;
  bool isSaved;

  PhotosInfo(this.uid);

  Map<String, dynamic> toMap() {
    return {
      'uid' : uid,
      'profilePhotoPath' : profilePhotoPath,
      'otherPhotoPaths' : otherPhotoPaths,
      'isSaved' : isSaved
    };
  }

  PhotosInfo photosInfoFromSnapshot(DocumentSnapshot snapshot) {
    PhotosInfo photosInfo = new PhotosInfo(uid);
    photosInfo.profilePhotoPath = snapshot.data['profilePhotoPath'];
    photosInfo.otherPhotoPaths = snapshot.data['otherPhotoPaths'];
    photosInfo.isSaved = snapshot.data['isSaved'];
    return photosInfo;
  }






}