import 'package:cloud_firestore/cloud_firestore.dart';

class PersonalInfo{

  String uid;
  String genotype;
  String ethnicGroup;
  String religion;
  bool isPartnerReligionImportant;
  bool isPartnerEthnicityImportant;
  String howOftenYouDrink;
  String howOftenYouSmoke;
  String hobbies;
  bool isSaved = false;

  PersonalInfo(this.uid);


  @override
  String toString() {
    return 'PersonalInfo{uid: $uid, genotype: $genotype, ethnicGroup: $ethnicGroup, religion: $religion, isPartnerReligionImportant: $isPartnerReligionImportant, isPartnerEthnicityImportant: $isPartnerEthnicityImportant, howOftenYouDrink: $howOftenYouDrink, howOftenYouSmoke: $howOftenYouSmoke, hobbies: $hobbies, isSaved: $isSaved}';
  }

  Map<String, dynamic> toMap() {
    return {
    'uid': uid,
    'genotype': genotype,
    'ethnicGroup': ethnicGroup,
    'religion': religion,
    'isPartnerReligionImportant': isPartnerReligionImportant,
    'isPartnerEthnicityImportant': isPartnerEthnicityImportant,
    'howOftenYouDrink': howOftenYouDrink,
    'howOftenYouSmoke': howOftenYouSmoke,
    'hobbies': hobbies,
    'isSaved': isSaved
    };
  }

  PersonalInfo personalInfoFromSnapshot(DocumentSnapshot snapshot){
    PersonalInfo personalInfo = new PersonalInfo(uid);
    personalInfo.genotype = snapshot.data['genotype'];
    personalInfo.ethnicGroup = snapshot.data['ethnicGroup'];
    personalInfo.religion = snapshot.data['religion'];
    personalInfo.isPartnerEthnicityImportant = snapshot.data['isPartnerEthnicityImportant'];
    personalInfo.isPartnerReligionImportant = snapshot.data['isPartnerReligionImportant'];
    personalInfo.howOftenYouDrink = snapshot.data['howOftenYouDrink'];
    personalInfo.howOftenYouSmoke = snapshot.data['howOftenYouSmoke'];
    personalInfo.hobbies = snapshot.data['hobbies'];
    personalInfo.isSaved = snapshot.data['isSaved'];
    return personalInfo;
  }
}